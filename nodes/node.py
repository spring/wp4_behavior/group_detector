#! /usr/bin/env python
from group_detector import GroupDetectorRosNode


if __name__ == '__main__':

    detector_node = GroupDetectorRosNode()
    detector_node.run()
    detector_node.close()

