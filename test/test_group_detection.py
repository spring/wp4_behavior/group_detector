#! /usr/bin/env python
import unittest
import numpy as np
import numpy.testing as npt
from group_detector import GroupDetection


class GroupDetectionCase(unittest.TestCase):

    def test_basic_group_detection(self):

        gd = GroupDetection(stride=1.0)

        # situation:   (p_1)-   -(p_2) (p_3)-
        persons = [
            ('p_3', (2.3, 0.0, 0.0)),
            ('p_1', (0.0, 0.0, 0.0)),
            ('p_2', (2.0, 0.0, np.pi)),
        ]
        groups = gd.detect(persons)

        assert len(groups) == 1
        assert len(groups[0].members) == 2
        assert 'p_1' in groups[0].members
        assert 'p_2' in groups[0].members
        npt.assert_almost_equal(groups[0].center, [1.0, 0.0])

        group_a_id = groups[0].id

        # situation:        (p_3)
        #                     |
        #              (p_1)-   -(p_2)
        persons = [
            ('p_1', (0.0, 0.0, 0.0)),
            ('p_2', (2.0, 0.0, np.pi)),
            ('p_3', (1.0, 1.0, -np.pi/2))
        ]
        groups = gd.detect(persons)

        assert len(groups) == 1
        assert len(groups[0].members) == 3
        assert 'p_1' in groups[0].members
        assert 'p_2' in groups[0].members
        assert 'p_3' in groups[0].members
        npt.assert_almost_equal(groups[0].center, [1.0, 0.0])
        assert groups[0].id == group_a_id


        # situation:        (p_3)                   (p_4)-
        #                     |
        #              (p_1)-   -(p_2)
        persons = [
            ('p_1', (0.0, 0.0, 0.0)),
            ('p_3', (1.0, 1.0, -np.pi/2)),
            ('p_4', (5.0, 1.0, 0.0)),
            ('p_2', (2.0, 0.0, np.pi)),
        ]
        groups = gd.detect(persons)

        assert len(groups) == 1
        assert len(groups[0].members) == 3
        assert 'p_1' in groups[0].members
        assert 'p_2' in groups[0].members
        assert 'p_3' in groups[0].members
        npt.assert_almost_equal(groups[0].center, [1.0, 0.0])
        assert groups[0].id == group_a_id


        # situation:        (p_3)                   (p_4)-   -(p_5)
        #                     |
        #              (p_1)-   -(p_2)
        persons = [
            ('p_1', (0.0, 0.0, 0.0)),
            ('p_4', (5.0, 1.0, 0.0)),
            ('p_2', (2.0, 0.0, np.pi)),
            ('p_3', (1.0, 1.0, -np.pi/2)),
            ('p_5', (7.0, 1.0, np.pi))
        ]
        groups = gd.detect(persons)

        assert len(groups) == 2

        if 'p_1' in groups[0].members:
            group_a_idx = 0
            group_b_idx = 1
        else:
            group_a_idx = 1
            group_b_idx = 0

        assert len(groups[group_a_idx].members) == 3
        assert 'p_1' in groups[group_a_idx].members
        assert 'p_2' in groups[group_a_idx].members
        assert 'p_3' in groups[group_a_idx].members
        npt.assert_almost_equal(groups[group_a_idx].center, [1.0, 0.0])
        assert groups[group_a_idx].id == group_a_id

        assert len(groups[group_b_idx].members) == 2
        assert 'p_4' in groups[group_b_idx].members
        assert 'p_5' in groups[group_b_idx].members
        npt.assert_almost_equal(groups[group_b_idx].center, [6.0, 1.0])
        group_b_id = groups[group_b_idx].id

        assert group_a_id != group_b_id

        # situation:        (p_3)                   (p_6)-   -(p_7)
        #                     |
        #              (p_1)-   -(p_2)
        persons = [
            ('p_1', (0.0, 0.0, 0.0)),
            ('p_7', (7.0, 1.0, np.pi)),
            ('p_2', (2.0, 0.0, np.pi)),
            ('p_6', (5.0, 1.0, 0.0)),
            ('p_3', (1.0, 1.0, -np.pi / 2)),
        ]
        groups = gd.detect(persons)

        assert len(groups) == 2

        if 'p_1' in groups[0].members:
            group_a_idx = 0
            group_b_idx = 1
        else:
            group_a_idx = 1
            group_b_idx = 0

        assert len(groups[group_a_idx].members) == 3
        assert 'p_1' in groups[group_a_idx].members
        assert 'p_2' in groups[group_a_idx].members
        assert 'p_3' in groups[group_a_idx].members
        npt.assert_almost_equal(groups[group_a_idx].center, [1.0, 0.0])
        assert groups[group_a_idx].id == group_a_id

        assert len(groups[group_b_idx].members) == 2
        assert 'p_6' in groups[group_b_idx].members
        assert 'p_7' in groups[group_b_idx].members
        npt.assert_almost_equal(groups[group_b_idx].center, [6.0, 1.0])
        assert groups[group_b_idx].id != group_b_id


        # situation:        (p_3)                   (p_2)-   -(p_7)
        #                     |
        #              (p_1)-   -(p_6)
        persons = [
            ('p_3', (1.0, 1.0, -np.pi / 2)),
            ('p_6', (2.0, 0.0, np.pi)),
            ('p_1', (0.0, 0.0, 0.0)),
            ('p_2', (5.0, 1.0, 0.0)),
            ('p_7', (7.0, 1.0, np.pi))
        ]
        groups = gd.detect(persons)

        assert len(groups) == 2

        if 'p_1' in groups[0].members:
            group_a_idx = 0
            group_b_idx = 1
        else:
            group_a_idx = 1
            group_b_idx = 0

        assert len(groups[group_a_idx].members) == 3
        assert 'p_1' in groups[group_a_idx].members
        assert 'p_6' in groups[group_a_idx].members
        assert 'p_3' in groups[group_a_idx].members
        npt.assert_almost_equal(groups[group_a_idx].center, [1.0, 0.0])
        assert groups[group_a_idx].id == group_a_id

        assert len(groups[group_b_idx].members) == 2
        assert 'p_2' in groups[group_b_idx].members
        assert 'p_7' in groups[group_b_idx].members
        npt.assert_almost_equal(groups[group_b_idx].center, [6.0, 1.0])
        assert groups[group_b_idx].id != group_b_id


if __name__ == '__main__':
    unittest.main()
