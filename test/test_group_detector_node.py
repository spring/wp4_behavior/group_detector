#! /usr/bin/env python
import sys
import unittest
import rostest
import rospy
import tf
import numpy as np
import numpy.testing as npt
from collections import namedtuple
from py_spring_hri import HRIListener
from hri_msgs.msg import IdsList
from std_msgs.msg import String


GROUPS_TOPIC_NAMESPACE = '/humans/groups/'
BODIES_TOPIC_NAMESPACE = '/humans/bodies/'
PERSONS_TOPIC_NAMESPACE = '/humans/persons/'


class ROSInterface:

    def __init__(self):

        #######################
        # publishers

        self._bodies_tracked_publisher = rospy.Publisher(BODIES_TOPIC_NAMESPACE + 'tracked', IdsList, queue_size=1, latch=True)

        self._persons_tracked_publisher = rospy.Publisher(PERSONS_TOPIC_NAMESPACE + 'tracked', IdsList, queue_size=1, latch=True)
        self._person_publishers = dict()

        self.tf_broadcaster = tf.TransformBroadcaster()

        # sleep to allow correct registration of publishers and subscribers
        rospy.sleep(1)


    def publish_tf(self, name, pos=None):

        if pos is None:
            pos = (0.0, 0.0, 0.0, 0.0)

        current_time = rospy.Time.now()
        quat = tf.transformations.quaternion_from_euler(0, 0, pos[3])
        self.tf_broadcaster.sendTransform(
            (pos[0], pos[1], pos[2]),
            quat,
            current_time,
            name,
            'map'
        )

    ###########################
    # BODIES

    def publish_tracked_bodies(self, body_ids):
        msg = IdsList()
        msg.ids = body_ids
        self._bodies_tracked_publisher.publish(msg)


    ###########################
    # PERSONS

    PersonPublishers = namedtuple('PersonPublishers', ['body_id'])

    def publish_persons_with_bodies(self, persons):

        person_ids = []
        body_ids = []
        for (person_id, position) in persons:

            body_id = 'b_' + person_id

            self.publish_tf('body_' + body_id, [position[0], position[1], 0.0, position[2]])

            self.publish_person(person_id=person_id, body_id=body_id)

            body_ids.append(body_id)
            person_ids.append(person_id)

        self.publish_tracked_bodies(body_ids)
        self.publish_tracked_persons(person_ids)


    def publish_person(self, person_id='person_1', body_id=''):

        if person_id not in self._person_publishers:

            body_id_pub = rospy.Publisher(
                PERSONS_TOPIC_NAMESPACE + '{}/body_id'.format(person_id),
                String,
                queue_size=1,
                latch=True
            )

            self._person_publishers[person_id] = ROSInterface.PersonPublishers(
                body_id_pub,
            )

        pubs = self._person_publishers[person_id]

        # body_id
        body_id_msg = String()
        body_id_msg.data = body_id
        pubs.body_id.publish(body_id_msg)

        return person_id


    def publish_tracked_persons(self, person_ids):
        msg = IdsList()
        msg.ids = person_ids
        self._persons_tracked_publisher.publish(msg)


class GroupDetectorNodeCase(unittest.TestCase):

    def __init__(self, *args):
        super().__init__(*args)

        rospy.init_node('test_group_detector_node', anonymous=True)

        # start ROS4HRI subscriber and publisher
        self.hri_sub = HRIListener.start_with_default_properties(default_target_frame='map')
        self.hri_pub = ROSInterface()


    def test_basic_group_detection(self):

        # no groups should be detected
        assert len(self.hri_sub.groups) == 0

        #######################################
        # situation:   (p_1)-   -(p_2) (p_3)-
        persons = [
            ('p_1', (0.0, 0.0, 0.0)),
            ('p_2', (2.0, 0.0, np.pi)),
            ('p_3', (2.3, 0.0, 0.0))
        ]

        # publish
        self.hri_pub.publish_persons_with_bodies(persons)
        rospy.sleep(0.5)

        # check
        with self.hri_sub.groups.lock:
            assert len(self.hri_sub.groups) == 1

            group_a_id = list(self.hri_sub.groups.keys())[0]

            assert len(self.hri_sub.groups[group_a_id].member_ids) == 2
            assert 'p_1' in self.hri_sub.groups[group_a_id].member_ids
            assert 'p_2' in self.hri_sub.groups[group_a_id].member_ids

            group_tf_translation = self.hri_sub.groups[group_a_id].transform().transform.translation
            npt.assert_almost_equal([group_tf_translation.x, group_tf_translation.y, group_tf_translation.z], [1.0, 0.0, 0.0])

        #######################################
        # situation:        (p_3)
        #                     |
        #              (p_1)-   -(p_2)
        persons = [
            ('p_1', (0.0, 0.0, 0.0)),
            ('p_2', (2.0, 0.0, np.pi)),
            ('p_3', (1.0, 1.0, -np.pi/2))
        ]

        # publish
        self.hri_pub.publish_persons_with_bodies(persons)
        rospy.sleep(0.5)

        # check
        with self.hri_sub.groups.lock:
            assert len(self.hri_sub.groups) == 1

            assert list(self.hri_sub.groups.keys())[0] == group_a_id

            assert len(self.hri_sub.groups[group_a_id].member_ids) == 3
            assert 'p_1' in self.hri_sub.groups[group_a_id].member_ids
            assert 'p_2' in self.hri_sub.groups[group_a_id].member_ids
            assert 'p_3' in self.hri_sub.groups[group_a_id].member_ids

            group_tf_translation = self.hri_sub.groups[group_a_id].transform().transform.translation
            npt.assert_almost_equal([group_tf_translation.x, group_tf_translation.y, group_tf_translation.z], [1.0, 0.0, 0.0])


        #######################################
        # situation:        (p_3)                   (p_4)-
        #                     |
        #              (p_1)-   -(p_2)
        persons = [
            ('p_1', (0.0, 0.0, 0.0)),
            ('p_3', (1.0, 1.0, -np.pi/2)),
            ('p_4', (5.0, 1.0, 0.0)),
            ('p_2', (2.0, 0.0, np.pi)),
        ]

        # publish
        self.hri_pub.publish_persons_with_bodies(persons)
        rospy.sleep(0.5)

        # check
        with self.hri_sub.groups.lock:
            assert len(self.hri_sub.groups) == 1

            assert list(self.hri_sub.groups.keys())[0] == group_a_id

            assert len(self.hri_sub.groups[group_a_id].member_ids) == 3
            assert 'p_1' in self.hri_sub.groups[group_a_id].member_ids
            assert 'p_2' in self.hri_sub.groups[group_a_id].member_ids
            assert 'p_3' in self.hri_sub.groups[group_a_id].member_ids

            group_tf_translation = self.hri_sub.groups[group_a_id].transform().transform.translation
            npt.assert_almost_equal([group_tf_translation.x, group_tf_translation.y, group_tf_translation.z], [1.0, 0.0, 0.0])


        #######################################
        # situation:        (p_3)                   (p_4)-   -(p_5)
        #                     |
        #              (p_1)-   -(p_2)
        persons = [
            ('p_1', (0.0, 0.0, 0.0)),
            ('p_4', (5.0, 1.0, 0.0)),
            ('p_2', (2.0, 0.0, np.pi)),
            ('p_3', (1.0, 1.0, -np.pi/2)),
            ('p_5', (7.0, 1.0, np.pi))
        ]

        # publish
        self.hri_pub.publish_persons_with_bodies(persons)
        rospy.sleep(0.5)

        # check
        with self.hri_sub.groups.lock:
            assert len(self.hri_sub.groups) == 2

            key0 = list(self.hri_sub.groups.keys())[0]
            key1 = list(self.hri_sub.groups.keys())[1]

            if 'p_1' in self.hri_sub.groups[key0].member_ids:
                group_a_id = key0
                group_b_id = key1
            else:
                group_a_id = key1
                group_b_id = key0

            assert len(self.hri_sub.groups[group_a_id].member_ids) == 3
            assert 'p_1' in self.hri_sub.groups[group_a_id].member_ids
            assert 'p_2' in self.hri_sub.groups[group_a_id].member_ids
            assert 'p_3' in self.hri_sub.groups[group_a_id].member_ids

            group_tf_translation = self.hri_sub.groups[group_a_id].transform().transform.translation
            npt.assert_almost_equal([group_tf_translation.x, group_tf_translation.y, group_tf_translation.z], [1.0, 0.0, 0.0])

            assert len(self.hri_sub.groups[group_b_id].member_ids) == 2
            assert 'p_4' in self.hri_sub.groups[group_b_id].member_ids
            assert 'p_5' in self.hri_sub.groups[group_b_id].member_ids

            group_tf_translation = self.hri_sub.groups[group_b_id].transform().transform.translation
            npt.assert_almost_equal([group_tf_translation.x, group_tf_translation.y, group_tf_translation.z], [6.0, 1.0, 0.0])

if __name__ == '__main__':
    rostest.rosrun('group_detection', 'test_group_detection_node', GroupDetectorNodeCase, sys.argv)