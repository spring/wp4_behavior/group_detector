# Group Detector

Author: Chris Reinke (chris.reinke@inria.fr)

version 0.0.3


Group detection assigns people that where detected in the surrounding of ARI to conversational groups. 
Detection of such groups is an essential element of SPRING due to two reasons. 
1) It defines the groups with which ARI can interact and identifies the people ARI has to consider when interacting with a group. 
2) It improves the human aware navigation as it allows to model group spaces. 
Group spaces are used to correctly join groups and to avoid interrupting them by navigating, for example, through them.

As a solution we use the Graph-Cuts for F-formation (GCFF) algorithm by Setti et al. (2015, F-formation detection: Individuating free-standing conversational groups in images). 
It is based on the concept of F-formations by Kendon (1990, Conducting interaction: Patterns of behavior in focused encounters). 
F-formations describe the arrangement of individuals of a group with respect to their positions and orientations. 
They are defined by three social spaces: o-space, p-space, and r-space. 
The o-space is an empty space around which the individuals of a group are positioned. 
The p-space is the space in which the members of a group are positioned. 
The r-space is the space outside the group. 

GCFF identifies the o-spaces of different groups. 
It uses for this the concept of transactional segments. 
These describe the space in front of a person in which it can perform actions and its sensing (vision and hearing) are best.
GCFF identifies areas in which the transactional segments of people overlap. 
These overlapping spaces are potential o-spaces of groups in which people interact with each othe

Technical documentation of the detector: [Deliverable D4.3: Multi-modal behaviour
recognition in realistic environments](https://spring-h2020.eu/wp-content/uploads/2023/07/SPRING_D4.3_Multi-modal-behaviour-recognition-in-realistic-environments_FFinal_23-06-2022.pdf) (Chapters 1.3.2 and 3.3)

## How to start

```
roslaunch group_detector node.launch
```

## How to test

Go in the test directory and use the unittest and rostest:
```
cd ./test
python test_group_detection.py -v
rostest test_group_detector_node.test
```

## Topics used

Node [/group_detector]
Publications: 
 * /humans/groups/tracked [hri_msgs/IdsList]
 * /rosout [rosgraph_msgs/Log]
 * /tf [tf2_msgs/TFMessage]

Subscriptions: 
 * /humans/bodies/tracked [hri_msgs/IdsList]
 * /humans/persons/known [hri_msgs/IdsList]
 * /humans/persons/tracked [hri_msgs/IdsList]
 * /tf [tf2_msgs/TFMessage]
 * /tf_static [tf2_msgs/TFMessage]

Services: 
 * /group_detector/get_loggers
 * /group_detector/set_logger_level
 * /group_detector/tf2_frames
