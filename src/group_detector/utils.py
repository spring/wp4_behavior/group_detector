import numpy as np

def group_labels_to_lists(group_labels):
    '''Converts a group labels (list with id of the group for each person) to a group list.'''

    group_labels = np.array(group_labels)

    group_list = []

    group_ids = set(group_labels)

    for group_id in group_ids:
        cur_group_inds = (group_labels == group_id)

        if np.sum(cur_group_inds) > 1:
            group_list.append(np.where(cur_group_inds)[0])

    return group_list

