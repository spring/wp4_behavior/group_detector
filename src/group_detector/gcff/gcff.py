# This file, part of Social MPC in the WP6 of the Spring project,
# is part of a project that has received funding from the
# European Union’s Horizon 2020 research and innovation programme
#  under grant agreement No 871245.
#
# Copyright (C) 2020-2022 by Inria
# Authors : Chris Reinke, Alex Auternaud, Timothée Wintz
# chris.reinke@inria.fr
# alex.auternaud@inria.fr
# timothee.wintz@inria.fr

import numpy as np
import pygco


class Group:
    def __init__(self, center, person_ids):
        self.center = center
        self.person_ids = person_ids


def identify_groups(persons, **parameters):
    # distance from to person to the center of its transactional space (aslo called D)
    stride = parameters.get('stride', 1.0)
    # minimum description length parameter (also called sigma)
    mdl = parameters.get('mdl', 1.0)
    max_iter = parameters.get('max_iter', 100)

    n_persons = persons.shape[0]

    # calculate the center of the transactional space for each person and the initinal group centers
    transaction_segments = np.full((n_persons, 2), np.nan)
    group_centers = np.full((n_persons, 2), np.nan)
    for p_idx in range(n_persons):
        ts_x = persons[p_idx][0] + np.cos(persons[p_idx][2]) * stride
        ts_y = persons[p_idx][1] + np.sin(persons[p_idx][2]) * stride

        transaction_segments[p_idx, :] = [ts_x, ts_y]
        group_centers[p_idx, :] = [ts_x, ts_y]

    labels = np.arange(n_persons)
    distances = np.zeros((n_persons, n_persons))

    pairwise_cost = np.zeros_like(distances)
    cost_v = np.zeros((0, n_persons))
    cost_h = np.zeros((1, n_persons - 1))
    label_cost = np.ones(n_persons) * mdl

    if n_persons == 1:
        result = [Group(center=group_centers[0, :], person_ids=[0])]
        return result

    iter_count = 0
    while iter_count < max_iter:

        # calculate distance of each person to each group center
        for p_idx in range(n_persons):
            distances[p_idx, :] = np.sqrt(
                np.sum((transaction_segments[p_idx] - group_centers) ** 2, 1))

        # perform graph cut for 1 iteration
        old_labels = labels
        labels = pygco.cut_grid_graph(np.array([distances]),
                                      pairwise_cost=pairwise_cost,
                                      cost_v=cost_v,
                                      cost_h=cost_h,
                                      label_cost=label_cost,
                                      n_iter=-1)

        # stop if there is no change of labels
        if np.all(old_labels == labels):
            break

        # recalculate the group centers
        for g_idx in range(group_centers.shape[0]):
            # people in that group
            people_in_group_inds = (labels == g_idx)

            if np.any(people_in_group_inds):
                group_centers[g_idx, :] = np.mean(
                    transaction_segments[people_in_group_inds, :], 0)

        iter_count += 1

    result = []

    for g_idx in range(group_centers.shape[0]):
        # people in that group
        people_in_group = np.where(labels == g_idx)[0].tolist()
        if len(people_in_group):
            result.append(
                Group(center=group_centers[g_idx, :], person_ids=people_in_group))

    return result
