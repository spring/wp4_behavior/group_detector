from .group_detector_ros_node import GroupDetectorRosNode
from .group_detection import GroupDetection

__version__ = '0.0.2'
