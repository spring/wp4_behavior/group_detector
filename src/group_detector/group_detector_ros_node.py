import rospy
import logging
from py_spring_hri import HRIListener
from .hri_publisher import HRIPublisher
from .group_detection import GroupDetection
import tf
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue


class GroupDetectorRosNode:

    def __init__(self):

        rospy.logdebug('start node')
        rospy.init_node('group_detection')

        rospy.logdebug('get parameters')
        # rate
        self.rate_in_hz = rospy.get_param('~rate', 10)
        # log level
        self.log_level = rospy.get_param('~loglevel', 'None')
        self.set_log_level(self.log_level)
        # params for gcff
        stride = rospy.get_param('~stride', 1.0)
        mdl = rospy.get_param('~mdl', 1.0)
        max_graphcut_iter = rospy.get_param('~max_graphcut_iter', 100)
        self.diag_timer_rate = rospy.get_param('~diag_timer_rate', 10)

        rospy.logdebug('init the hri_listener')
        # subscribe to tracked persons and bodies, but no properties as we only need their TF
        self.hri_listener = HRIListener(subscribe_persons=True, subscribe_bodies=True)

        rospy.logdebug('init the hri_publisher')
        self.hri_publisher = HRIPublisher()

        self.group_detection = GroupDetection(stride=stride, mdl=mdl, max_graphcut_iter=max_graphcut_iter)
        
        self._publishers = []
        self._subscribers = []
        self._timers = []
        
        self._timers.append(rospy.Timer(rospy.Duration(1/self.diag_timer_rate), callback=self._diagnostics_callback))
        self._diagnostics_pub = rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=10)
        self._publishers.append(self._diagnostics_pub)

        self.groups = []
        self.persons_data = []


    def set_log_level(self, log_level):

        if log_level == '0' or log_level.lower() == 'debug':
            log_level = rospy.DEBUG
        elif log_level == '1' or log_level.lower() == 'info':
            log_level = rospy.INFO
        elif log_level == '2' or log_level.lower() == 'warn':
            log_level = rospy.WARN
        elif log_level == '3' or log_level.lower() == 'fatal':
            log_level = rospy.FATAL
        elif log_level.lower() == 'none':
            log_level = None
        else:
            raise ValueError('Unknown log level: {}!'.format(log_level))

        if log_level is not None:
            logger = logging.getLogger('rosout')
            logger.setLevel(rospy.impl.rosout._rospy_to_logging_levels[log_level])

        self.log_level = log_level


    def run(self):

        self.hri_listener.start()

        rospy.logdebug('start detection loop with rate of %s Hz', self.rate_in_hz)
        rate = rospy.Rate(self.rate_in_hz)

        # run until node is closed
        while not rospy.is_shutdown():

            rospy.logdebug('new group detection loop iteration')

            rospy.logdebug('collect data from tracked persons')
            self.persons_data = []

            timestamp = rospy.Time.now()
            self.groups = []

            with self.hri_listener.tracked_persons.lock:

                # if self.hri_listener.tracked_persons.header and self.hri_listener.tracked_persons.header.stamp > rospy.Time(0):
                #     timestamp = self.hri_listener.tracked_persons.header.stamp

                for person in self.hri_listener.tracked_persons.values():
                    if person.body:
                        person_tf = person.body.transform(target_frame='map')
                        if person_tf:

                            rotation = person_tf.transform.rotation
                            (_, _, yaw) = tf.transformations.euler_from_quaternion([rotation.x, rotation.y, rotation.z, rotation.w])

                            translation = person_tf.transform.translation
                            person_position = (translation.x, translation.y, yaw)

                            self.persons_data.append((person.id, person_position))

            #rospy.logwarn(self.persons_data)

            rospy.logdebug('detect groups')
            # return list of Group tuples with (id, members, position)

            # rospy.logwarn(self.persons_data)
            self.groups = self.group_detection.detect(self.persons_data)

            # rospy.logwarn(groups)

            rospy.logdebug('publish groups')
            group_ids = []
            for group in self.groups:
                self.hri_publisher.publish_group_members(group.id, group.members, timestamp)
                self.hri_publisher.publish_group_tf(group.id, group.center, timestamp)
                group_ids.append(group.id)
            self.hri_publisher.unregister_group_members(group_ids)

            #rospy.logwarn(group_ids)
            self.hri_publisher.publish_tracked_groups(group_ids, timestamp)

            rospy.logdebug('sleep for %s seconds', rate.remaining().to_sec())
            rate.sleep()


    def _diagnostics_callback(self, event):
        self.publish_diagnostics()


    def publish_diagnostics(self):
        diagnostic_msg = DiagnosticArray()
        diagnostic_msg.status = []
        diagnostic_msg.header.stamp = rospy.Time.now()
        status = DiagnosticStatus()
        status.name = 'Functionality: AV Perception: Group Detector'
        status.values.append(KeyValue(key='Number of tracked persons', value='{}'.format(len(self.hri_listener.tracked_persons))))
        status.values.append(KeyValue(key='Number of tracked persons with data', value='{}'.format(len(self.persons_data))))
        status.values.append(KeyValue(key='Number of group detected', value='{}'.format(len(self.groups))))

        # by default
        status.level = DiagnosticStatus.OK
        status.message = ''
        if len(self.hri_listener.tracked_persons) != len(self.persons_data):
            status.level = DiagnosticStatus.OK
            status.message = 'Number of tracked persons {} is different from number of tracked persons with correct data (id and tf) {}'.format(len(self.hri_listener.tracked_persons), len(self.persons_data))
        if not self.hri_listener.tracked_persons:
            status.level = DiagnosticStatus.OK
            status.message = 'No tracked persons as input'
        
        diagnostic_msg.status.append(status)
        self._diagnostics_pub.publish(diagnostic_msg)


    def close(self):
        rospy.logdebug('end group detection')
        self.hri_listener.close()
        self.hri_publisher.close()
        if self._subscribers:
            for subscriber in self._subscribers:
                subscriber.unregister()
        if self._publishers:
            for publisher in self._publishers:
                if isinstance(publisher, dict):
                    for pub in publisher.values():
                        pub.unregister()
                else:
                    publisher.unregister()
        if self._timers:
            for timer in self._timers:
                timer.shutdown()


if __name__ == '__main__':

    detection_node = GroupDetectorRosNode()
    detection_node.run()
    detection_node.close()