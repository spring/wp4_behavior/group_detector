import numpy as np
import scipy.optimize
from .gcff import identify_groups
from collections import namedtuple


Group = namedtuple('Group', ['id', 'members', 'center'])


def sdc(a, b):
    """Sorensen-Dice coefficent between sets."""
    return 2 * len(a.intersection(b)) / (len(a) + len(b))


def compute_pairwise_sdc(sets_a, sets_b):
    sim_matrix = np.empty((len(sets_a), len(sets_b)))

    for i, set_i in enumerate(sets_a):
        for j, set_j in enumerate(sets_b):
            sim_matrix[i, j] = sdc(set_i, set_j)

    return sim_matrix


def compute_set_matches(sets_a, sets_b, cost_limit=0.0):
    """Computes matches between two discrete sets based on the Sorensen-Dice coefficent between the individual sets."""

    cost_matrix = -1 * compute_pairwise_sdc(sets_a, sets_b)

    row_ind, col_ind = scipy.optimize.linear_sum_assignment(cost_matrix)

    # # only calculate the matches, not the unmatched ones
    # matches = list(zip(row_ind, col_ind))

    matches = []
    # lists with unmatched elements start with all unmatched
    # un_matched_a = np.arange(len(sets_a))
    # un_matched_b = np.arange(len(sets_b))
    for idx, cur_row_ind in enumerate(row_ind):
        cur_col_ind = col_ind[idx]

        # cost limit for assignment is 0
        if cost_matrix[cur_row_ind, cur_col_ind] < cost_limit:
            matches.append((cur_row_ind, cur_col_ind))

    #         # remove match from unmatched lists
    #         un_matched_a[cur_row_ind] = -1
    #         un_matched_b[cur_col_ind] = -1
    #
    # un_matched_a = un_matched_a[un_matched_a >= 0]
    # un_matched_b = un_matched_b[un_matched_b >= 0]

    # return matches, un_matched_a, un_matched_b
    return matches


class GroupDetection:

    def __init__(self, stride=1.0, mdl=1.0, max_graphcut_iter=100):
        self.stride = stride
        self.mdl = mdl
        self.max_graphcut_iter = max_graphcut_iter

        self.previous_group_ids = None
        self.previous_group_member_ids_per_group = None
        self.last_generated_group_id = None


    def detect(self, persons):
        """Detects groups from a list of persons

        Args:
            persons (list): List of persons with (person_id, position), the position is a tuple with (x, y, orientation)

        Returns: dict with group_id as keys and (group_members, position) as items
        """

        group_ids = []
        group_member_ids_per_group = []
        detected_groups = []

        # there are only groups if there are more than 1 person
        if len(persons) > 1:

            # matrix where each person is a row with (x, y, theta)
            person_positions = np.empty((len(persons), 3))
            for person_idx, (_, person_position) in enumerate(persons):
                person_positions[person_idx, :] = person_position

            # print(person_positions)

            # identify groups which are a list of Group(center=group_centers[g_idx, :], person_ids=people_in_group))
            identified_groups = identify_groups(person_positions, stride=self.stride, mdl=self.mdl, max_iter=self.max_graphcut_iter)

            # print('\nIdentified Groups:')
            # for group in identified_groups:
            #     print('Group:')
            #     print('\tPerson_ids: {}'.format(group.person_ids))
            #     print('\tCenter: {}'.format(group.center))

            # get list with sets of person ids per group
            # need to reassign correct person_id's as the identify_groups function returns the idxs in the person_positions list
            group_member_ids_per_group = []
            group_centers = []
            for group in identified_groups:

                if len(group.person_ids) > 1:
                    person_ids = set()
                    for person_idx in group.person_ids:
                        person_ids.add(persons[person_idx][0])

                    group_member_ids_per_group.append(person_ids)
                    group_centers.append(group.center)

            # print('\ngroup_member_ids_per_group: {}'.format(group_member_ids_per_group))

            group_ids = [None] * len(group_member_ids_per_group)

            # give groups an id, compute consistent ids based on the persons that were previously detected in groups
            if self.previous_group_member_ids_per_group and group_member_ids_per_group:

                group_ids = [None] * len(group_member_ids_per_group)

                matches_with_group_idx = compute_set_matches(self.previous_group_member_ids_per_group, group_member_ids_per_group)

                # assign matched groups the corresponding id from the previous group
                for (prev_group_idx, cur_group_idx) in matches_with_group_idx:
                    group_ids[cur_group_idx] = self.previous_group_ids[prev_group_idx]

            # generate new group ids for the new groups (groups that have no match)
            for cur_group_idx, cur_group_id in enumerate(group_ids):
                if cur_group_id is None:
                    group_ids[cur_group_idx] = self.generate_group_id()

            # generate output format:  list with Group tuples (group_id, members, center) as items
            for group_idx, group_id in enumerate(group_ids):
                detected_groups.append(Group(
                    group_id,
                    group_member_ids_per_group[group_idx],  # ids of members
                    group_centers[group_idx]  # center
                ))

        # keep memory of previous groups
        self.previous_group_ids = group_ids
        self.previous_group_member_ids_per_group = group_member_ids_per_group

        return detected_groups


    def generate_group_id(self):

        if self.last_generated_group_id is None:
            self.last_generated_group_id = np.random.randint(10000)
        else:
            self.last_generated_group_id += 1

        # TODO: generate a non-repeating group id
        return 'grp' + str(self.last_generated_group_id)
