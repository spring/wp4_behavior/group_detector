#! /usr/bin/env python
import rospy
from hri_msgs.msg import IdsList
import tf

ROS4HRI_GROUPS_NAMESPACE = '/humans/groups'
ROS4HRI_GROUPS_TF_TEMPLATE = 'group_{}'


class HRIPublisher:

    def __init__(self):

        self._tracked_groups_publisher = rospy.Publisher(
            ROS4HRI_GROUPS_NAMESPACE + '/tracked',
            IdsList,
            queue_size=1,
            latch=True
        )

        self._group_members_publishers = dict()

        self._tf_broadcaster = tf.TransformBroadcaster()

        # sleep to give enough time to register the publishers / subscriber
        rospy.sleep(1.0)


    def publish_tracked_groups(self, group_ids, stamp):
        msg = IdsList()
        msg.header.stamp = stamp
        msg.ids = group_ids

        rospy.logdebug('publish tracked groups: {}'.format(group_ids))

        self._tracked_groups_publisher.publish(msg)


    def publish_group_members(self, group_id, member_ids, stamp):

        if group_id in self._group_members_publishers:
            pub = self._group_members_publishers[group_id]
        else:
            pub = rospy.Publisher(
                ROS4HRI_GROUPS_NAMESPACE + '/{}/member_ids'.format(group_id),
                IdsList,
                queue_size=1,
                # latch=True
            )
            self._group_members_publishers[group_id] = pub
            # sleep to give enough time to register the publishers / subscriber
            # rospy.sleep(1.0)

        # define message
        msg = IdsList()
        msg.header.stamp = stamp
        msg.ids = member_ids

        rospy.logdebug('Publish groups {} members: {}'.format(group_id, member_ids))

        pub.publish(msg)


    def unregister_group_members(self, group_ids):
        for group_id in list(self._group_members_publishers.keys()):
            if group_id not in group_ids:
                self._group_members_publishers[group_id].unregister()
                del self._group_members_publishers[group_id]


    def publish_group_tf(self, group_id, pos, time=None, target_frame='map'):

        tf_name = ROS4HRI_GROUPS_TF_TEMPLATE.format(group_id)

        if time is None:
            time = rospy.Time.now()

        rospy.logdebug('Publish tf for group {} at: {}'.format(group_id, pos))

        self._tf_broadcaster.sendTransform(
            (pos[0], pos[1], 0),
            tf.transformations.quaternion_from_euler(0, 0, 0),
            time,
            tf_name,
            target_frame
        )


    def close(self):
        self._tracked_groups_publisher.unregister()
        for pub in self._group_members_publishers.values():
            pub.unregister()

